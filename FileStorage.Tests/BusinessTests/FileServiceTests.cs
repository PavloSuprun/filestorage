﻿using FileStorageBLL.Models;
using FileStorageBLL.Services;
using FileStorageBLL.Validation;
using FileStorageDAL.Data.Entities;
using FileStorageDAL.Entities;
using FileStorageDAL.Interfaces;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileStorage.Tests.BusinessTests
{
    public class FileServiceTests
    {
        [Test]
        public async Task FileService_AddAsync_AddsFile()
        {
            //arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.FileRepository.AddAsync(It.IsAny<File>()));

            var fileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var file = new FileModel { Id = 1, Name = "file", Path = "/path", UserId = 1 };

            //act
            await fileService.AddAsync(file);

            //assert
            mockUnitOfWork.Verify(x => x.FileRepository.AddAsync(It.Is<File>(c => c.Id == file.Id && c.AppUserId == file.UserId && c.Name == file.Name && c.Path == file.Path)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public async Task FileService_GetAll_ReturnsAllFiles()
        {
            //arrange
            var expected = FileModels.ToList();
            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.FileRepository.GetAllWithDetailsAsync())
                .ReturnsAsync(FileEntities.AsEnumerable());

            var fileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            var actual = await fileService.GetAllAsync();

            //assert
            actual.Should().BeEquivalentTo(expected);
        }

        [TestCase(1)]
        [TestCase(2)]
        public async Task FileService_GetById_ReturnsFileModel(int id)
        {
            //arrange
            var expected = FileModels.FirstOrDefault(x => x.Id == id);

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(x => x.FileRepository.GetByIdWithDetailsAsync(It.IsAny<int>()))
                .ReturnsAsync(FileEntities.FirstOrDefault(x => x.Id == id));

            var fileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            var actual = await fileService.GetByIdAsync(1);

            //assert
            actual.Should().BeEquivalentTo(expected);
        }

        [Test]
        public async Task FileService_UpdateAsync_UpdatesFile()
        {
            //arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.FileRepository.Update(It.IsAny<File>()));

            var fileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var file = new FileModel { Id = 1, Name = "newName", Path = "/path", UserId = 1 };

            //act
            await fileService.UpdateAsync(file);

            //assert
            mockUnitOfWork.Verify(x => x.FileRepository.Update(It.Is<File>(c => c.Id ==file.Id && c.Name == file.Name && c.Path == file.Path && c.AppUserId == file.UserId)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public async Task FileService_DeleteAsync_ThrowsFileStorageExceptionWhenIdNotFound()
        {
            //arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.FileRepository.DeleteByIdAsync(It.IsAny<int>()));
            var fileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            Func<Task> act = async () => await fileService.DeleteAsync(1);

            //assert
            await act.Should().ThrowAsync<FileStorageException>();
        }

        [TestCase(1, new[] { 1, 2 })]
        [TestCase(2, new[] { 3, 4 })]
        public async Task FileService_GetByFilterAsync_ReturnsFilesByUser(int userId, IEnumerable<int> expectedFilesIds)
        {
            //arrange
            var expected = FileModels.Where(x => expectedFilesIds.Contains(x.Id));
            var filter = new FilterSearchModel { UserId = userId };

            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.FileRepository.GetAllWithDetailsAsync())
                .ReturnsAsync(FileEntities.AsEnumerable());

            var fileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            var actual = await fileService.GetByFilterAsync(filter);

            //assert
            actual.Should().BeEquivalentTo(expected);
        }

        [TestCase("File #1", new[] { 1 })]
        [TestCase("File", new[] { 1, 2, 3, 4 })]
        public async Task FileService_GetByFilterAsync_ReturnsFilesName(string fileName, IEnumerable<int> expectedFilesIds)
        {
            //arrange
            var expected = FileModels.Where(x => expectedFilesIds.Contains(x.Id));
            var filter = new FilterSearchModel { Name = fileName };

            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.FileRepository.GetAllWithDetailsAsync())
                .ReturnsAsync(FileEntities.AsEnumerable());

            var fileService = new FileService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            var actual = await fileService.GetByFilterAsync(filter);

            //assert
            actual.Should().BeEquivalentTo(expected);
        }

        private static IEnumerable<FileModel> FileModels =>
            new List<FileModel>
            {
                new FileModel { Id = 1, Name = "File #1", UserId = 1, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#1" },
                new FileModel { Id = 2, Name = "File #2", UserId = 1, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#2" },
                new FileModel { Id = 3, Name = "File #3", UserId = 2, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#3" },
                new FileModel { Id = 4, Name = "File #4", UserId = 2, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#4" }
            };

        private static IEnumerable<File> FileEntities =>
            new List<File>
            {
                new File { Id = 1, Name = "File #1", AppUserId = 1, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#1", AppUser = new AppUser { Id = 1 } },
                new File { Id = 2, Name = "File #2", AppUserId = 1, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#2", AppUser = new AppUser { Id = 1 } },
                new File { Id = 3, Name = "File #3", AppUserId = 2, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#3", AppUser = new AppUser { Id = 2 } },
                new File { Id = 4, Name = "File #4", AppUserId = 2, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#4", AppUser = new AppUser { Id = 2 } }
            };

    }
}
