﻿using FileStorageBLL.Models;
using FileStorageBLL.Services;
using FileStorageBLL.Validation;
using FileStorageDAL.Entities;
using FileStorageDAL.Interfaces;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileStorage.Tests.BusinessTests
{
    public class AppUserServiceTests
    {
        [Test]
        public async Task AppUserService_AddAsync_AddsUser()
        {
            //arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.AppUserRepository.AddAsync(It.IsAny<AppUser>()));

            var appUserService = new AppUserService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var appUser = new AppUserModel { Id = 1, FullName = "Username", Email = "random@gmail.com", Password = "Rndm_123", RoleId = 1};

            //act
            await appUserService.AddAsync(appUser);

            //assert
            mockUnitOfWork.Verify(x => x.AppUserRepository.AddAsync(It.Is<AppUser>(c => c.Id == appUser.Id && c.FullName == appUser.FullName && c.Password == appUser.Password && c.Email == appUser.Email && c.RoleId == appUser.RoleId)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public async Task AppUserService_GetAll_ReturnsAllUsers()
        {
            //arrange
            var expected = AppUserModels.ToList();
            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.AppUserRepository.GetAllWithDetailsAsync())
                .ReturnsAsync(AppUserEntities.AsEnumerable());

            var appUserService = new AppUserService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            var actual = await appUserService.GetAllAsync();

            //assert
            actual.Should().BeEquivalentTo(expected);
        }

        [TestCase(1)]
        [TestCase(2)]
        public async Task AppUserService_GetById_ReturnsAppUserModel(int id)
        {
            //arrange
            var expected = AppUserModels.FirstOrDefault(x => x.Id == id);

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(x => x.AppUserRepository.GetByIdWithDetailsAsync(It.IsAny<int>()))
                .ReturnsAsync(AppUserEntities.FirstOrDefault(x => x.Id == id));

            var appUserService = new AppUserService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            var actual = await appUserService.GetByIdAsync(1);

            //assert
            actual.Should().BeEquivalentTo(expected);
        }

        [Test]
        public async Task AppUserService_UpdateAsync_UpdatesAppUser()
        {
            //arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.AppUserRepository.Update(It.IsAny<AppUser>()));

            var appUserService = new AppUserService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var appUser = new AppUserModel { Id = 1, Email = "user1@gmail.com", FullName = "NewName", Password = "Password1", RoleId = 1 };

            //act
            await appUserService.UpdateAsync(appUser);

            //assert
            mockUnitOfWork.Verify(x => x.AppUserRepository.Update(It.Is<AppUser>(c => c.Id == appUser.Id && c.Email == appUser.Email && c.FullName == appUser.FullName && c.RoleId == appUser.RoleId)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public async Task AppUserService_GetRoles_ReturnsAllRoles()
        {
            //arrange
            var expected = AppRoleModels.ToList();
            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.RoleRepository.GetAllAsync())
                .ReturnsAsync(AppRoleEntities.AsEnumerable());

            var appUserService = new AppUserService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            var actual = await appUserService.GetRoles();

            //assert
            actual.Should().BeEquivalentTo(expected);
        }

        [TestCase("email@gmail.com", "qwerty")]
        public async Task AppUserService_Login_VerifiesUsersCredentials(string email, string password)
        {
            //arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.AppUserRepository.GetAllWithDetailsAsync())
                .ReturnsAsync(AppUserEntities.AsEnumerable());

            var appUserService = new AppUserService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            Func<Task> act = async () => await appUserService.Login(new AppUserModel() { Email = email, Password = password });

            //assert
            await act.Should().ThrowAsync<FileStorageException>();
        }

        [Test]
        public async Task AppUserService_DeleteAsync_ThrowsFileStorageExceptionWhenIdNotFound()
        {
            //arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.AppUserRepository.DeleteByIdAsync(It.IsAny<int>()));
            var appUserService = new AppUserService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            Func<Task> act = async () => await appUserService.DeleteAsync(1);

            //assert
            await act.Should().ThrowAsync<FileStorageException>();
        }

        [Test]
        public async Task AppUserService_AddRoleAsync_AddsRole()
        {
            //arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.RoleRepository.AddAsync(It.IsAny<AppRole>()));

            var appUserService = new AppUserService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var appRoleModel = new AppRoleModel { Id = 1, Name = "Adman"};

            //act
            await appUserService.AddRoleAsync(appRoleModel);

            //assert
            mockUnitOfWork.Verify(x => x.RoleRepository.AddAsync(It.Is<AppRole>(c => c.Id == appRoleModel.Id && c.RoleName == appRoleModel.Name)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        private static IEnumerable<AppUserModel> AppUserModels =>
            new List<AppUserModel>
            {
                new AppUserModel { Id = 1, Email = "user1@gmail.com", FullName = "User 1", Password = "Password1", RoleId = 1, FilesIds = new int[] { } },
                new AppUserModel { Id = 2, Email = "user2@gmail.com", FullName = "User 2", Password = "Password2", RoleId = 2, FilesIds = new int[] { } }               
            };

        private static IEnumerable<AppUser> AppUserEntities =>
            new List<AppUser>
            {
                new AppUser { Id = 1, Email = "user1@gmail.com", FullName = "User 1", Password = "Password1", RoleId = 1 },
                new AppUser { Id = 2, Email = "user2@gmail.com", FullName = "User 2", Password = "Password2", RoleId = 2 }
            };

        private static IEnumerable<string> AppRoleModels =>
            new List<string>
            {
                "Admin",
                "User"
            };

        private static IEnumerable<AppRole> AppRoleEntities =>
            new List<AppRole>
            {
                new AppRole { Id = 1, RoleName = "Admin" },
                new AppRole { Id = 2, RoleName = "User" }
            };
    }
}
