﻿using FileStorageDAL.Data.Entities;
using FileStorageDAL.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace FileStorage.Tests
{
    internal class AppRoleEqualityComparer : IEqualityComparer<AppRole>
    {
        public bool Equals([AllowNull] AppRole x, [AllowNull] AppRole y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.RoleName == y.RoleName;
        }

        public int GetHashCode([DisallowNull] AppRole obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class AppUserEqualityComparer : IEqualityComparer<AppUser>
    {
        public bool Equals([AllowNull] AppUser x, [AllowNull] AppUser y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.RoleId == y.RoleId
                && x.FullName == y.FullName
                && x.Email == y.Email
                && x.Password == y.Password;
        }

        public int GetHashCode([DisallowNull] AppUser obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class FileEqualityComparer : IEqualityComparer<File>
    {
        public bool Equals([AllowNull] File x, [AllowNull] File y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.Name == y.Name
                && x.Path == y.Path
                && x.AppUserId == y.AppUserId;
        }

        public int GetHashCode([DisallowNull] File obj)
        {
            return obj.GetHashCode();
        }
    }
}
