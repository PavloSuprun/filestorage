﻿using FileStorageDAL.Data;
using FileStorageDAL.Data.Entities;
using FileStorageDAL.Entities;
using FileStorageDAL.Repositories;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileStorage.Tests.DataTests
{
    [TestFixture]
    public class AppUserRepositoryTests
    {
        private FileStorageDbContext _context;
        private AppUserRepository _repository;

        [TestCase(1)]
        [TestCase(2)]
        public async Task AppUserRepository_GetByIdAsync_ReturnsSingleValue(int id)
        {
            //arrange
            var expected = ExpectedUsers.FirstOrDefault(x => x.Id == id);

            //act
            var user = await _repository.GetByIdAsync(id);

            //assert
            Assert.That(user, Is.EqualTo(expected)
                .Using(new AppUserEqualityComparer()), message: "GetByIdAsync method works incorrect");
        }

        [Test]
        public async Task AppUserRepository_GetAllAsync_ReturnsAllValues()
        {
            //act
            var users = await _repository.GetAllAsync();

            //assert
            Assert.That(users, Is.EqualTo(ExpectedUsers).Using(new AppUserEqualityComparer()), message: "GetAllAsync method works incorrect");
        }

        [Test]
        public async Task AppUserRepository_AddAsync_AddsValueToDatabase()
        {
            //arrange
            var user = new AppUser { Id = 3 };

            //act
            await _repository.AddAsync(user);
            await _context.SaveChangesAsync();

            //assert
            Assert.That(_context.AppUsers.Count(), Is.EqualTo(3), message: "AddAsync method works incorrect");
        }

        [Test]
        public async Task AppUserRepository_Delete_DeletesEntity()
        {
            //act
            _repository.Delete(ExpectedUsers.First());
            await _context.SaveChangesAsync();

            //assert
            Assert.That(_context.AppUsers.Count(), Is.EqualTo(1), message: "Delete works incorrect");
        }

        [Test]
        public async Task AppUserRepository_DeleteByIdAsync_DeletesEntity()
        {
            //act
            await _repository.DeleteByIdAsync(1);
            await _context.SaveChangesAsync();

            //assert
            Assert.That(_context.AppUsers.Count(), Is.EqualTo(1), message: "DeleteByIdAsync works incorrect");
        }

        [Test]
        public async Task AppUserRepository_Update_UpdatesEntity()
        {
            //arrange
            var user = new AppUser { Id = 1, FullName = "New Name 123" };

            //act
            _repository.Update(user);
            await _context.SaveChangesAsync();

            //assert
            Assert.That(user, Is.EqualTo(new AppUser { Id = 1, FullName = "New Name 123" })
                .Using(new AppUserEqualityComparer()), message: "Update method works incorrect");
        }

        [Test]
        public async Task AppUserRepository_GetAllWithDetailsAsync_ReturnsWithIncludedEntities()
        {
            //arrange
            var users = await _repository.GetAllWithDetailsAsync();

            //act
            Assert.That(users,
                Is.EqualTo(ExpectedUsers).Using(new AppUserEqualityComparer()), message: "GetAllWithDetailsAsync method works incorrect");

            //assert
            Assert.That(users.SelectMany(u => u.Files).OrderBy(f => f.Id),
                Is.EqualTo(ExpectedFiles).Using(new FileEqualityComparer()), message: "GetByIdWithDetailsAsync method doesnt't return included entities");
        }

        [Test]
        public async Task AppUserRepository_GetByIdWithDetailsAsync_ReturnsWithIncludedEntities()
        {
            //arrange
            var expected = ExpectedUsers.FirstOrDefault(x => x.Id == 1);

            //act
            var user = await _repository.GetByIdWithDetailsAsync(1);

            //assert
            Assert.That(user, Is.EqualTo(expected)
                .Using(new AppUserEqualityComparer()), message: "GetByIdWithDetailsAsync method works incorrect");

            Assert.That((user.Files.OrderBy(f => f.Id)), Is.EqualTo(ExpectedFiles.Where(f => f.AppUserId == expected.Id))
                .Using(new FileEqualityComparer()), message: "GetByIdWithDetailsAsync method doesnt't return included entities"); ;
        }

        [SetUp]
        public void Setup()
        {
            _context = new FileStorageDbContext(UnitTestHelper.GetUnitTestDbOptions());
            _repository = new AppUserRepository(_context);
        }
        private static IEnumerable<AppUser> ExpectedUsers =>
            new[]
            {
                new AppUser { Id = 1, Email = "user1@gmail.com", FullName = "User 1", Password = "Password1", RoleId = 1 },
                new AppUser { Id = 2, Email = "user2@gmail.com", FullName = "User 2", Password = "Password2", RoleId = 2 }
            };

        private static IEnumerable<File> ExpectedFiles =>
            new[]
            {
               new File { Id = 1, Name = "File #1", AppUserId = 1, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#1" },
                new File { Id = 2, Name = "File #2", AppUserId = 1, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#2" },
                new File { Id = 3, Name = "File #3", AppUserId = 2, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#3" },
                new File { Id = 4, Name = "File #4", AppUserId = 2, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#4" }
            };
    }
}
