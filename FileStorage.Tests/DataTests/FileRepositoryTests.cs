﻿using FileStorageDAL.Data;
using FileStorageDAL.Data.Entities;
using FileStorageDAL.Entities;
using FileStorageDAL.Repositories;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileStorage.Tests.DataTests
{
    [TestFixture]
    public class FileRepositoryTests
    {
        private FileStorageDbContext _context;
        private FileRepository _repository;

        [TestCase(1)]
        [TestCase(2)]
        public async Task FileRepository_GetByIdAsync_ReturnsSingleValue(int id)
        {
            //arrange
            var expected = ExpectedFiles.FirstOrDefault(x => x.Id == id);

            //act
            var file = await _repository.GetByIdAsync(id);        

            //assert
            Assert.That(file, Is.EqualTo(expected)
                .Using(new FileEqualityComparer()), message: "GetByIdAsync method works incorrect");
        }

        [Test]
        public async Task FileRepository_GetAllAsync_ReturnsAllValues()
        {
            //act
            var files = await _repository.GetAllAsync();

            //assert
            Assert.That(files, Is.EqualTo(ExpectedFiles).Using(new FileEqualityComparer()), message: "GetAllAsync method works incorrect");
        }

        [Test]
        public async Task FileRepository_AddAsync_AddsValueToDatabase()
        {
            //arrange
            var file = new File { Id = 5 };

            //act
            await _repository.AddAsync(file);
            await _context.SaveChangesAsync();

            //assert
            Assert.That(_context.Files.Count(), Is.EqualTo(5), message: "AddAsync method works incorrect");
        }

        [Test]
        public async Task FileRepository_Delete_DeletesEntity()
        {
            //act
            _repository.Delete(ExpectedFiles.First());
            await _context.SaveChangesAsync();

            //assert
            Assert.That(_context.Files.Count(), Is.EqualTo(3), message: "Delete works incorrect");
        }

        [Test]
        public async Task FileRepository_DeleteByIdAsync_DeletesEntity()
        {
            //act
            await _repository.DeleteByIdAsync(1);
            await _context.SaveChangesAsync();

            //assert
            Assert.That(_context.Files.Count(), Is.EqualTo(3), message: "DeleteByIdAsync works incorrect");
        }

        [Test]
        public async Task FileRepository_Update_UpdatesEntity()
        {
            //arrange
            var file = new File { Id = 1, Name = "New Name 123" };

            //act
            _repository.Update(file);
            await _context.SaveChangesAsync();

            //assert
            Assert.That(file, Is.EqualTo(new File { Id = 1, Name = "New Name 123" })
                .Using(new FileEqualityComparer()), message: "Update method works incorrect");
        }

        [Test]
        public async Task FileRepository_GetAllWithDetailsAsync_ReturnsWithIncludedEntities()
        {
            //act
            var files = await _repository.GetAllWithDetailsAsync();

            //assert
            Assert.That(files,
                Is.EqualTo(ExpectedFiles).Using(new FileEqualityComparer()), message: "GetAllWithDetailsAsync method works incorrect");

            Assert.That(files.GroupBy(f => f.AppUserId).Select(f => f.First()).Select(u => u.AppUser).OrderBy(i => i.Id),
                Is.EqualTo(ExpectedUsers).Using(new AppUserEqualityComparer()), message: "GetByIdWithDetailsAsync method doesnt't return included entities");
        }

        [Test]
        public async Task FileRepository_GetByIdWithDetailsAsync_ReturnsWithIncludedEntities()
        {
            //arrange
            var expected = ExpectedFiles.FirstOrDefault(x => x.Id == 1);

            //act
            var file = await _repository.GetByIdWithDetailsAsync(1);

            //assert
            Assert.That(file, Is.EqualTo(expected)
                .Using(new FileEqualityComparer()), message: "GetByIdWithDetailsAsync method works incorrect");

            Assert.That(file.AppUser, Is.EqualTo(ExpectedUsers.FirstOrDefault(x => x.Id == expected.AppUserId))
                .Using(new AppUserEqualityComparer()), message: "GetByIdWithDetailsAsync method doesnt't return included entities");
        }


        [SetUp]
        public void Setup()
        {
            _context = new FileStorageDbContext(UnitTestHelper.GetUnitTestDbOptions());
            _repository = new FileRepository(_context);
        }

        private static IEnumerable<File> ExpectedFiles =>
            new[]
            {
                new File { Id = 1, Name = "File #1", AppUserId = 1, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#1" },
                new File { Id = 2, Name = "File #2", AppUserId = 1, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#2" },
                new File { Id = 3, Name = "File #3", AppUserId = 2, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#3" },
                new File { Id = 4, Name = "File #4", AppUserId = 2, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#4" }
            };

        private static IEnumerable<AppUser> ExpectedUsers =>
            new[]
            {
                new AppUser { Id = 1, Email = "user1@gmail.com", FullName = "User 1", Password = "Password1", RoleId = 1 },
                new AppUser { Id = 2, Email = "user2@gmail.com", FullName = "User 2", Password = "Password2", RoleId = 2 }
            };
    }
}
