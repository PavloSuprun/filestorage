﻿using FileStorageDAL.Data;
using FileStorageDAL.Entities;
using FileStorageDAL.Repositories;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileStorage.Tests.DataTests
{
    [TestFixture]
    public class AppRoleRepositoryTests
    {
        private FileStorageDbContext _context;
        private AppRoleRepository _repository;

        [TestCase(1)]
        [TestCase(2)]
        public async Task AppRoleRepository_GetByIdAsync_ReturnsSingleValue(int id)
        {
            //arrange
            var expected = ExpectedAppRoles.FirstOrDefault(x => x.Id == id);

            //act
            var appRole = await _repository.GetByIdAsync(id);

            //assert
            Assert.That(appRole, Is.EqualTo(expected).Using(new AppRoleEqualityComparer()), message: "GetByIdAsync method works incorrect");
        }

        [Test]
        public async Task AppRoleRepository_GetAllAsync_ReturnsAllValues()
        {
            //act
            var appRoles = await _repository.GetAllAsync();

            //assert
            Assert.That(appRoles, Is.EqualTo(ExpectedAppRoles).Using(new AppRoleEqualityComparer()), message: "GetAllAsync method works incorrect");
        }

        [Test]
        public async Task AppRoleRepository_AddAsync_AddsValueToDatabase()
        {
            //arrange
            var appRole = new AppRole { Id = 3 };

            //act
            await _repository.AddAsync(appRole);
            await _context.SaveChangesAsync();

            //assert
            Assert.That(_context.AppRoles.Count(), Is.EqualTo(3), message: "AddAsync method works incorrect");
        }

        [Test]
        public async Task AppRoleRepository_Delete_DeletesEntity()
        {
            //act
            _repository.Delete(ExpectedAppRoles.First());
            await _context.SaveChangesAsync();

            //assert
            Assert.That(_context.AppRoles.Count(), Is.EqualTo(1), message: "Delete works incorrect");
        }

        [Test]
        public async Task AppRoleRepository_DeleteByIdAsync_DeletesEntity()
        {
            //act
            await _repository.DeleteByIdAsync(1);
            await _context.SaveChangesAsync();

            //assert
            Assert.That(_context.AppRoles.Count(), Is.EqualTo(1), message: "DeleteByIdAsync works incorrect");
        }

        [Test]
        public async Task AppRoleRepository_Update_UpdatesEntity()
        {
            //arrange
            var appRole = new AppRole { Id = 1, RoleName = "Moderator" };

            //act
            _repository.Update(appRole);
            await _context.SaveChangesAsync();

            //assert
            Assert.That(appRole, Is.EqualTo(new AppRole { Id = 1, RoleName = "Moderator" })
                .Using(new AppRoleEqualityComparer()), message: "Update method works incorrect");
        }

        [SetUp]
        public void Setup()
        {
            _context = new FileStorageDbContext(UnitTestHelper.GetUnitTestDbOptions());
            _repository = new AppRoleRepository(_context);
        }

        private static IEnumerable<AppRole> ExpectedAppRoles =>
            new[]
            {
                new AppRole { Id = 1, RoleName = "Admin" },
                new AppRole { Id = 2, RoleName = "User" }
            };
    }
}
