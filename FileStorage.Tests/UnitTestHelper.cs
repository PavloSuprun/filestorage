﻿using AutoMapper;
using FileStorageBLL.Mapper;
using FileStorageDAL.Data;
using FileStorageDAL.Data.Entities;
using FileStorageDAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace FileStorage.Tests
{
    internal static class UnitTestHelper
    {
        public static DbContextOptions<FileStorageDbContext> GetUnitTestDbOptions()
        {
            var options = new DbContextOptionsBuilder<FileStorageDbContext>()            
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var context = new FileStorageDbContext(options))
            {
                SeedData(context);
            }

            return options;
        }

        public static IMapper CreateMapperProfile()
        {
            var myProfile = new AutomapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

            return new Mapper(configuration);
        }

        public static void SeedData(FileStorageDbContext context)
        {
            context.AppRoles.AddRange(
                new AppRole { Id = 1, RoleName = "Admin" },
                new AppRole { Id = 2, RoleName = "User" });
            context.AppUsers.AddRange(
                new AppUser { Id = 1, Email = "user1@gmail.com", FullName = "User 1", Password = "Password1", RoleId = 1 },
                new AppUser { Id = 2, Email = "user2@gmail.com", FullName = "User 2", Password = "Password2", RoleId = 2 });
            context.Files.AddRange(
                new File { Id = 1, Name = "File #1", AppUserId = 1, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#1" },
                new File { Id = 2, Name = "File #2", AppUserId = 1, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#2" },
                new File { Id = 3, Name = "File #3", AppUserId = 2, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#3" },
                new File { Id = 4, Name = "File #4", AppUserId = 2, Path = @"C:\Users\supru\source\repos\filestoragepl\FileStoragePL\wwwroot\uploads\File_#4" });
            context.SaveChanges();
        } 
    }
}
