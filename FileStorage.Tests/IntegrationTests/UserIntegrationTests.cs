﻿using FileStorageBLL.Models;
using FileStoragePL.Models;
using FluentAssertions;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FileStorage.Tests.IntegrationTests
{
    public class UserIntegrationTests
    {
        private CustomWebApplicationFactory _factory;
        private HttpClient _client;
        private const string RequestUri = "api/user/";

        [SetUp]
        public void Init()
        {
            _factory = new CustomWebApplicationFactory();
            _client = _factory.CreateClient();
        }

        [Test]
        public async Task UserController_Register_AddsUserToDb()
        {
            //arrange
            var user = new RegistrationBindingModel
            {
                FullName = "name",
                Email = "mail@gmail.com",
                Password = "password",
                Role = "Admin"            
            };

            var content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");

            //act
            var httpResponse = await _client.PostAsync(RequestUri + "Registration", content);

            //assert
            httpResponse.EnsureSuccessStatusCode();
        }

        [Test]
        public async Task UserController_Get_ReturnsUnauthorized()
        {
            // act
            var httpResponse = await _client.GetAsync(RequestUri + "Users");

            // assert
            var res = httpResponse.IsSuccessStatusCode;
            res.Should().BeFalse();
        }

        [Test]
        public async Task UserController_Delete_ReturnsUnauthorized()
        {
            // act
            var httpResponse = await _client.DeleteAsync(RequestUri + "Delete");

            // assert
            var res = httpResponse.IsSuccessStatusCode;
            res.Should().BeFalse();
        }

        [TestCase("mail@gmail.com", "qwerty")]
        public async Task UserController_Login_VerifiesUserCredentials(string email, string password)
        {
            //arrange
            var content = new StringContent(JsonConvert.SerializeObject(new AppUserModel() { Email = email, Password = password }), Encoding.UTF8, "application/json");

            // act
            var httpResponse = await _client.PostAsync(RequestUri + "Login", content);

            // assert
            var res = httpResponse.IsSuccessStatusCode;
            res.Should().BeTrue();
        }

        [TearDown]
        public void TearDown()
        {
            _factory.Dispose();
            _client.Dispose();
        }
    }
}
