﻿using FileStorageBLL.Models;
using FileStorageDAL.Data;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace FileStorage.Tests.IntegrationTests
{
    public class FileIntegrationTests
    {
        private CustomWebApplicationFactory _factory;
        private HttpClient _client;
        private const string RequestUri = "api/file/";

        [SetUp]
        public void Init()
        {
            _factory = new CustomWebApplicationFactory();
            _client = _factory.CreateClient();
        }

        [Test]
        public async Task FileController_Upload_ReturnsErrorIfUnsupportedMediaType()
        {
            //arrange
            var file = new FileModel
            {
                Id = 3,
                Name = "file",
                Path = "/path",
                UserId = 1
            };

            var content = new StringContent(JsonConvert.SerializeObject(file), Encoding.UTF8, "application/json");

            //act
            var httpResponse = await _client.PostAsync(RequestUri + "Upload", content);

            //assert
            var res = httpResponse.IsSuccessStatusCode;
            res.Should().BeFalse();
        }

        [Test]
        public async Task FileController_Delete_ReturnsErrorIfWrongFileName()
        {
            // act
            var httpResponse = await _client.DeleteAsync(RequestUri + "Delete");

            // assert
            var res = httpResponse.IsSuccessStatusCode;
            res.Should().BeFalse();
        }

        [Test]
        public async Task FileController_Files_ReturnsSuccess()
        {
            //act
            var httpResponse = await _client.GetAsync(RequestUri + "Files");

            //assert
            httpResponse.EnsureSuccessStatusCode();
        }

        [Test]
        public async Task FileController_Download_ReturnsErrorIfWrongFileName()
        {
            // act
            var httpResponse = await _client.DeleteAsync(RequestUri + "Download");

            // assert
            var res = httpResponse.IsSuccessStatusCode;
            res.Should().BeFalse();
        }

        [TearDown]
        public void TearDown()
        {
            _factory.Dispose();
            _client.Dispose();
        }
    }
}
