import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Constants } from 'src/app/constants/constants';
import { File } from 'src/app/models/file';
import { ProgressStatus, ProgressStatusEnum } from 'src/app/models/progress-status';
import { User } from 'src/app/models/user';
import { AlertService } from 'src/app/services/alert.service';
import { FileService } from 'src/app/services/file.service';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss']
})
export class FileComponent implements OnInit {

  @BlockUI('main-loader') blockUI: NgBlockUI;
  @Input() public file: File;
  filepage: File;
  public fileInDownload: string;
  public percentage: number;
  public showProgress: boolean;
  public showDownloadError: boolean;
  public showUploadError: boolean;
  public showDeleteError: boolean;

  constructor(
    private service: FileService,
    private route: ActivatedRoute,
    private router: Router,
    private alert: AlertService
    ) { }

  get user():User{
    return JSON.parse(localStorage.getItem(Constants.USER_KEY)) as User;
  }

  ngOnInit(): void {

    if(this.file){
      this.getFile();
    }
    else{
      this.route.params.subscribe((params: Params) => this.filepage = JSON.parse(params['file']));
    }

  }

  private getFile() {
    this.blockUI.start();
    this.service.getFile(this.file.id).subscribe(
      data => {
        this.file = data;
        this.blockUI.stop();
      }
    );
  }

  public switcher() {
    this.filepage.isPublic = !this.filepage.isPublic;
    this.service.updateFile(this.filepage).subscribe();
  }

  public getLink() {
    navigator.clipboard.writeText("http://localhost:4200/file/" + JSON.stringify(this.filepage));
    this.alert.success("Copied to clipboard");
  }

  public downloadStatus(event: ProgressStatus) {
    switch (event.status) {
      case ProgressStatusEnum.START:
        this.showDownloadError = false;
        break;
      case ProgressStatusEnum.IN_PROGRESS:
        this.showProgress = true;
        this.percentage = event.percentage;
        break;
      case ProgressStatusEnum.COMPLETE:
        this.showProgress = false;
        break;
      case ProgressStatusEnum.ERROR:
        this.showProgress = false;
        this.showDownloadError = true;
        break;
    }
  }

  public deleteStatus(event: ProgressStatus) {
    switch (event.status) {
      case ProgressStatusEnum.START:
        this.showDeleteError = false;
        break;
      case ProgressStatusEnum.IN_PROGRESS:
        this.showProgress = true;
        this.percentage = event.percentage;
        break;
      case ProgressStatusEnum.COMPLETE:
        this.showProgress = false;
        this.router.navigate(["/file-management"])
        break;
      case ProgressStatusEnum.ERROR:
        this.showDeleteError = true;
        break;
    }
  }

}
