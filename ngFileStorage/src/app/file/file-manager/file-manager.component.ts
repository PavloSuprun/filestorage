import { Component, OnInit } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ProgressStatus, ProgressStatusEnum } from '../../models/progress-status';
import { FileService } from '../../services/file.service';
import { File } from 'src/app/models/file';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { Constants } from 'src/app/constants/constants';

@Component({
  selector: 'app-file-manager',
  templateUrl: './file-manager.component.html',
  styleUrls: ['./file-manager.component.scss']
})
export class FileManagerComponent implements OnInit {

  @BlockUI('main-loader') blockUI: NgBlockUI;
  public files: File[];
  public fileInDownload: string;
  public percentage: number;
  public showProgress: boolean;
  public showDownloadError: boolean;
  public showUploadError: boolean;
  public showDeleteError: boolean;
  searchStr = ''

  constructor(private service: FileService,
    private router:Router) { }

  ngOnInit() {
    this.getFiles();
  }

  get user():User{
    return JSON.parse(localStorage.getItem(Constants.USER_KEY)) as User;
  }

  private getFiles() {
    this.blockUI.start();
    this.service.getFiles(this.user).subscribe(
      data => {
        this.files = data;
        this.blockUI.stop();
      }
    );
  }

  public getFile(file: File){
    var temp = {file:JSON.stringify(file)};
    this.router.navigate(["/file", {file:JSON.stringify(file)}]);
  }

  public downloadStatus(event: ProgressStatus) {
    switch (event.status) {
      case ProgressStatusEnum.START:
        this.showDownloadError = false;
        break;
      case ProgressStatusEnum.IN_PROGRESS:
        this.showProgress = true;
        this.percentage = event.percentage;
        break;
      case ProgressStatusEnum.COMPLETE:
        this.showProgress = false;
        break;
      case ProgressStatusEnum.ERROR:
        this.showProgress = false;
        this.showDownloadError = true;
        break;
    }
  }

  public uploadStatus(event: ProgressStatus) {
    switch (event.status) {
      case ProgressStatusEnum.START:
        this.showUploadError = false;
        break;
      case ProgressStatusEnum.IN_PROGRESS:
        this.showProgress = true;
        this.percentage = event.percentage;
        break;
      case ProgressStatusEnum.COMPLETE:
        this.showProgress = false;
        this.getFiles();
        break;
      case ProgressStatusEnum.ERROR:
        this.showProgress = false;
        this.showUploadError = true;
        break;
    }
  }

  public deleteStatus(event: ProgressStatus) {
    switch (event.status) {
      case ProgressStatusEnum.START:
        this.showDeleteError = false;
        break;
      case ProgressStatusEnum.IN_PROGRESS:
        this.showProgress = true;
        this.percentage = event.percentage;
        break;
      case ProgressStatusEnum.COMPLETE:
        this.showProgress = false;
        this.getFiles();
        break;
      case ProgressStatusEnum.ERROR:
        this.showDeleteError = true;
        break;
    }
  }
}
