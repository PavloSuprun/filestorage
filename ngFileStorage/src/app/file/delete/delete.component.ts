import { HttpEventType } from '@angular/common/http';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';
import { ProgressStatus, ProgressStatusEnum } from '../../models/progress-status';
import { FileService } from '../../services/file.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent {

  @Input() public disabled: boolean;
  @Input() public fileId: number;
  @Output() public deleteStatus: EventEmitter<ProgressStatus>;

  constructor(private service: FileService,
    private alert: AlertService) {
    this.deleteStatus = new EventEmitter<ProgressStatus>();
  }

  public delete() {
    this.deleteStatus.emit( {status: ProgressStatusEnum.START});
    this.service.deleteFile(this.fileId).subscribe(
      data => {
        switch (data.type) {
          case HttpEventType.Response:
            this.deleteStatus.emit( {status: ProgressStatusEnum.COMPLETE});
            this.alert.success("File was deleted");
        }
        },
      error => {
        this.deleteStatus.emit( {status: ProgressStatusEnum.ERROR});
      }
    );
  }

}
