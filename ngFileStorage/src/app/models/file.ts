export class File{
  public id:number;
  public name:string;
  public userId:number;
  public isPublic: boolean;

  constructor(id:number, name:string, userId:number, isPublic:boolean){
    this.id = id;
    this.name = name;
    this.userId = userId;
    this.isPublic = isPublic;
  }
}