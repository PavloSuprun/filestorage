import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Constants } from './constants/constants';
import { File } from './models/file';
import { User } from './models/user';
import { FileService } from './services/file.service';
import { BlockUiTemplateComponent } from './sharedModule/block-ui-template/block-ui-template.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @BlockUI('main-loader') blockUI: NgBlockUI;
  public blockUiTemplateComponent = BlockUiTemplateComponent;
  title = 'ngFileStorage';

  constructor(public router:Router,
    private fileService: FileService) {

    if(window.location.href.indexOf("http://localhost:4200/file/") > -1){
      let uri = decodeURI( window.location.href.replace("http://localhost:4200/file/", ''));
      this.router.navigate(["/file", {file:uri}]);
    }
    else if(this.isUserLogin){
      this.router.navigate(["/file-management"])
    }
    else{
      this.router.navigate(["/login"])
    }
  }
  onLogout(){
    localStorage.removeItem(Constants.USER_KEY);
  }
  get isUserLogin(){
    const user = localStorage.getItem(Constants.USER_KEY);

    return user && user.length > 0;
  }

  get user():User{
    return JSON.parse(localStorage.getItem(Constants.USER_KEY)) as User;
  }

  get isAdmin():boolean{
    return this.user.roleId === 1;
  }

  get isUser():boolean{
    return this.user.roleId != 1 && !this.isAdmin;
  }
}
