import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BlockUIModule } from 'ng-block-ui';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './user/login/login.component';
import { RegisterComponent } from './user/register/register.component';
import { UserManagementComponent } from './user/user-management/user-management.component';
import { AllUserManagementComponent } from './user/all-user-management/all-user-management.component';
import { BlockUiTemplateComponent } from './sharedModule/block-ui-template/block-ui-template.component';
import { UploadComponent } from './file/upload/upload.component';
import { DownloadComponent } from './file/download/download.component';
import { FileManagerComponent } from './file/file-manager/file-manager.component';
import { DeleteComponent } from './file/delete/delete.component';
import { DeleteUserComponent } from './user/delete-user/delete-user.component';
import { SearchPipe } from './services/search.pipe';
import { AlertComponent } from './sharedModule/alert/alert.component';
import { AlertService } from './services/alert.service';
import { FileComponent } from './file/file/file.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    UserManagementComponent,
    AllUserManagementComponent,
    BlockUiTemplateComponent,
    UploadComponent,
    DownloadComponent,
    FileManagerComponent,
    DeleteComponent,
    DeleteUserComponent,
    SearchPipe,
    AlertComponent,
    FileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BlockUIModule.forRoot({
      template: BlockUiTemplateComponent
    })
  ],
  entryComponents:[BlockUiTemplateComponent],
  providers: [AlertService],
  bootstrap: [AppComponent]
})
export class AppModule { }
