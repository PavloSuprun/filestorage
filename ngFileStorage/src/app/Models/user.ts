export class User{
  public id?: number;
  public fullName:string;
  public email:string;
  public filesIds:number[];
  public roles:string[]=[];
  public roleId?:number;

  constructor(id:number = 0, fullName:string = '', email:string = '', roleId:number = 0, fileIds:number[]){
    this.id=id;
    this.fullName=fullName;
    this.email=email;
    this.roleId=roleId;
    this.filesIds=fileIds;
  }
}