import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllUserManagementComponent } from './user/all-user-management/all-user-management.component';
import { FileManagerComponent } from './file/file-manager/file-manager.component';
import { AuthGuardService } from './services/auth.service';
import { LoginComponent } from './user/login/login.component';
import { RegisterComponent } from './user/register/register.component';
import { UserManagementComponent } from './user/user-management/user-management.component';
import { FileComponent } from './file/file/file.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'user-management', component: UserManagementComponent, canActivate:[AuthGuardService] },
  { path: 'all-user-management', component: AllUserManagementComponent, canActivate:[AuthGuardService] },
  { path: 'file-management', component: FileManagerComponent, canActivate:[AuthGuardService] },
  { path: 'file', component: FileComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
