import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ResponseCode } from '../../enums/responseCode';
import { ResponseModel } from '../../models/responseModel';
import { Role } from '../../models/role';
import { AlertService } from '../../services/alert.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public roles:Role[]=[];

  public registerForm=this.formBuilder.group({
    fullName:['',[Validators.required, Validators.minLength(4)]],
    email:['',[Validators.email, Validators.required]],
    password:['', [Validators.required, Validators.minLength(4)]]
  })
  constructor(
    private formBuilder:FormBuilder,
    private userService:UserService,
    private alert: AlertService,
    private router:Router
    ) { }

  ngOnInit(): void {
    this.getAllRoles();
  }

  onSubmit(){
    let fullName = this.registerForm.controls['fullName'].value ? this.registerForm.controls['fullName'].value : '';
    let email = this.registerForm.controls['email'].value ? this.registerForm.controls['email'].value : '';
    let password = this.registerForm.controls['password'].value ? this.registerForm.controls['password'].value : '';

    this.userService.register(fullName, email, password,this.roles.filter(x=>x.isSelected).map(x=>x.role)[0]).subscribe((data:ResponseModel)=>{

    if(data.responseCode==ResponseCode.Ok){
      this.registerForm.controls['fullName'].setValue("");
      this.registerForm.controls['email'].setValue("");
      this.registerForm.controls['password'].setValue("");
      this.roles.forEach(x=>x.isSelected=false);
      this.alert.success('Registration is successfull');
      this.router.navigate(["/file-management"]);
    }
    else{
      this.alert.danger("Email is already owned");
    }
    }),(error: any)=>{
      console.log("error", error);
      this.alert.danger(error);
    }
  }

  getAllRoles(){
    this.userService.getAllRole().subscribe(roles=>{
      this.roles = roles;
    });
  }

  OnRoleChange(role:string){
    this.roles.forEach(x=>{
      if(x.role==role){
        x.isSelected=!x.isSelected;
      }
    })
  }

  get isRoleSelected(){
    return this.roles.filter(x=>x.isSelected).length>0;
  }

}
