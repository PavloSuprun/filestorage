import { Component, OnInit } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ProgressStatus, ProgressStatusEnum } from '../../models/progress-status';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-all-user-management',
  templateUrl: './all-user-management.component.html',
  styleUrls: ['./all-user-management.component.scss']
})
export class AllUserManagementComponent implements OnInit {

  @BlockUI('main-loader') blockUI: NgBlockUI;
  public userList:User[]=[];
  public showDeleteError: boolean;

  constructor(private userServise:UserService) { }

  ngOnInit(): void {
    this.getAllUser();
  }

  getAllUser(){
    this.blockUI.start();
    this.userServise.getAllUser().subscribe((data:any)=>{
      this.userList=data;
      this.blockUI.stop();
    })

  }

  public deleteStatus(event: ProgressStatus) {
    switch (event.status) {
      case ProgressStatusEnum.START:
        this.showDeleteError = false;
        break;
      case ProgressStatusEnum.IN_PROGRESS:
        break;
      case ProgressStatusEnum.COMPLETE:
        this.getAllUser();
        break;
      case ProgressStatusEnum.ERROR:
        this.showDeleteError = true;
        break;
    }
  }
}
