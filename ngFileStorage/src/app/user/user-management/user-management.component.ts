import { Component, OnInit } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {
  @BlockUI('main-loader') blockUI: NgBlockUI;

  public userList:User[]=[];

  constructor(private userServise:UserService) { }

  ngOnInit(): void {
    this.getAllUser();
  }

  getAllUser(){
    this.blockUI.start();
    this.userServise.getUserList().subscribe((data:any)=>{
      this.userList=data;
      this.blockUI.stop();
    })
  }
}
