import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Constants } from '../../constants/constants';
import { ResponseModel } from '../../models/responseModel';
import { AlertService } from '../../services/alert.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @BlockUI('main-loader') blockUI: NgBlockUI;
  public loginForm=this.formBuilder.group({
    email:['',[Validators.email, Validators.required]],
    password:['', Validators.required]
  })
  constructor(
    private formBuilder:FormBuilder,
    private userService:UserService,
    private router:Router,
    private alert: AlertService
    ) { }

  ngOnInit(): void {
  }

  onSubmit(){
    this.blockUI.start();

    let email = this.loginForm.controls['email'].value ? this.loginForm.controls['email'].value : '';
    let password = this.loginForm.controls['password'].value ? this.loginForm.controls['password'].value : '';

    this.userService.login(email, password).subscribe((data: ResponseModel)=>{
      if(data.responseCode==1){
        localStorage.setItem(Constants.USER_KEY, JSON.stringify(data.dataSet));
        this.router.navigate(["/file-management"]);
      }
      else{
        this.alert.danger("Wrong credentials");
      }
      this.blockUI.stop();
      console.log("response", data);
    }),(error: any)=>{
      this.blockUI.stop();
      console.log("error", error);
    }
  }
}
