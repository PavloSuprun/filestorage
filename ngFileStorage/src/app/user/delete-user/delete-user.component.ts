import { HttpEventType } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';
import { ProgressStatus, ProgressStatusEnum } from '../../models/progress-status';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.scss']
})
export class DeleteUserComponent {

  @Input() public user: User;
  @Output() public deleteStatus: EventEmitter<ProgressStatus>;

  constructor(private service:UserService,
    private alert:AlertService) {
    this.deleteStatus = new EventEmitter<ProgressStatus>();
  }


  public delete() {
    this.deleteStatus.emit( {status: ProgressStatusEnum.START});
    this.service.deleteUser(this.user.id).subscribe(
      data => {
        switch (data.type) {
          case HttpEventType.Response:
            this.deleteStatus.emit( {status: ProgressStatusEnum.COMPLETE});
            this.alert.success("User was deleted");
          }
        },
      error => {
        this.deleteStatus.emit( {status: ProgressStatusEnum.ERROR});
      }
    );
  }

}
