import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { ResponseCode } from '../enums/responseCode';
import { Constants } from '../constants/constants';
import { ResponseModel } from '../models/responseModel';
import { Role } from '../models/role';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly baseURL:string="https://localhost:44320/api/user/"

  constructor(private httpClient: HttpClient) { }

  public login(email:string, password:string)
  {
    const body={
      Email:email,
      Password:password
    }
    return this.httpClient.post<ResponseModel>(this.baseURL+"Login", body);
  }

  public register(fullName:string, email:string, password:string, role:string)
  {
    const body={
      fullName:fullName,
      Email:email,
      Password:password,
      Role:role
    }
    return this.httpClient.post<ResponseModel>(this.baseURL+"Registration", body);
  }

  public getAllUser(){
    let userInfo = JSON.parse(localStorage.getItem(Constants.USER_KEY));
    const headers=new HttpHeaders({
      'Authorization':`Bearer ${ userInfo?.token }`
    });

    return this.httpClient.get<ResponseModel>(this.baseURL+"AllUsers", {headers:headers})
    .pipe(map(res=>{
      let userList = new Array<User>();
      if(res.responseCode==ResponseCode.Ok)
      {
        if(res.dataSet)
        {
          res.dataSet.map((x:User)=>{
            userList.push(new User(x.id, x.fullName, x.email, x.roleId, x.filesIds))
          })
        }
      }
      return userList;
    }));
  }

  public getUserList(){
    let userInfo = JSON.parse(localStorage.getItem(Constants.USER_KEY));
    const headers=new HttpHeaders({
      'Authorization':`Bearer ${ userInfo?.token }`
    });

    return this.httpClient.get<ResponseModel>(this.baseURL+"Users", {headers:headers})
    .pipe(map(res=>{
      let userList = new Array<User>();
      if(res.responseCode==ResponseCode.Ok)
      {
        if(res.dataSet)
        {

          res.dataSet.map((x:User)=>{
            userList.push(new User(x.id, x.fullName, x.email, x.roleId, x.filesIds))
          })
        }
      }
      return userList;
    }));
  }

  public getAllRole(){
    let userInfo = JSON.parse(localStorage.getItem(Constants.USER_KEY));
    const headers=new HttpHeaders({
      'Authorization':`Bearer ${ userInfo?.token }`
    });

    return this.httpClient.get<ResponseModel>(this.baseURL+"Roles", {headers:headers})
    .pipe(map(res=>{
      let roleList = new Array<Role>();
      if(res.responseCode==ResponseCode.Ok)
      {
        if(res.dataSet)
        {
          res.dataSet.map((x:string)=>{
            roleList.push(new Role(x))
          })
        }
      }
      return roleList;
    }));
  }

  public deleteUser (id:number) {
    return this.httpClient.request(new HttpRequest(
      'DELETE',
      `${this.baseURL+"Delete"}?id=${id}`,
      null,
      {
        reportProgress: true
      }));
  }
}
