import { HttpClient, HttpEvent, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Constants } from '../constants/constants';
import { ResponseCode } from '../enums/responseCode';
import { ResponseModel } from '../models/responseModel';
import { File } from '../models/file';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  private baseApiUrl: string;
  private apiDownloadUrl: string;
  private apiUploadUrl: string;
  private apiDeleteUrl: string;
  private apiFileUrl: string;
  private apiFilesUrl: string;
  private apiUpdateUrl: string;
  private user: User = JSON.parse(localStorage.getItem(Constants.USER_KEY)) as User

  constructor(private httpClient: HttpClient) {
    this.baseApiUrl = 'https://localhost:44320/api/file/';
    this.apiDownloadUrl = this.baseApiUrl + 'Download';
    this.apiUploadUrl = this.baseApiUrl + 'Upload';
    this.apiDeleteUrl = this.baseApiUrl + 'Delete';
    this.apiFileUrl = this.baseApiUrl + 'File';
    this.apiFilesUrl = this.baseApiUrl + 'Files';
    this.apiUpdateUrl = this.baseApiUrl + 'Update';
  }

  public downloadFile(fileName: string): Observable<HttpEvent<Blob>> {
    return this.httpClient.request(new HttpRequest(
      'GET',
      `${this.apiDownloadUrl}?fileName=${fileName}`,
      null,
      {
        reportProgress: true,
        responseType: 'blob'
      }));
  }

  public uploadFile(file: Blob): Observable<HttpEvent<void>> {
    let params = new HttpParams().set('userId', this.user.id);
    let formData = new FormData();
    formData.append('file', file);

    return this.httpClient.request(new HttpRequest(
      'POST',
      this.apiUploadUrl,
      formData,
      {
        reportProgress: true,
        params: params
      },
      ));
  }

  public getFile(fileId: number): Observable<File> {
    return this.httpClient.get<ResponseModel>(`${this.apiFileUrl}?fileId=${fileId}`)
    .pipe(map(res => {
      let file: File;
      if(res.responseCode == ResponseCode.Ok)
      {
        if(res.dataSet)
        {
          file = res.dataSet;
        }
      }
      return file;
    }));
  }

  public getFiles(user: User): Observable<File[]> {

    return this.httpClient.get<ResponseModel>(this.apiFilesUrl)
    .pipe(map(res => {
      let fileList = new Array<File>();
      if(res.responseCode == ResponseCode.Ok)
      {
        if(res.dataSet)
        {
          res.dataSet.map((x:File)=>{
            if(user.roleId === 1 || x.isPublic || x.userId === user.id)
            fileList.push(new File(x.id, x.name, x.userId, x.isPublic))
          })
        }
      }
      return fileList;
    }));
  }

  public updateFile(file: File) {
    return this.httpClient.post<ResponseModel>( this.apiUpdateUrl, file)
    .pipe(map(res => {
      let file: File;
      if(res.responseCode == ResponseCode.Ok)
      {
        if(res.dataSet)
        {
          file = res.dataSet;
        }
      }
      return file;
    }));
  }

  public deleteFile (fileId: number) {
    return this.httpClient.request(new HttpRequest(
      'POST',
      `${this.apiDeleteUrl}?fileId=${fileId}`,
      null,
      {
        reportProgress: true
      }));
  }
}
