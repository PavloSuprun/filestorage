import {Pipe, PipeTransform} from '@angular/core';


@Pipe({
  name: 'searchFiles'
})
export class SearchPipe implements PipeTransform {
  transform(files: any[], search = ''): any[] {
    if (!search.trim()) {
      return files
    }

    return files.filter((file) => {
      return file.name.toLowerCase().includes(search.toLowerCase())
    })
  }

}
