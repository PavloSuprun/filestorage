﻿using FileStorageBLL.Interfaces;
using FileStorageBLL.Models;
using FileStorageBLL.Validation;
using FileStoragePL.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using System.IO;
using System.Threading.Tasks;

namespace FileStoragePL.Controllers
{
    /// <summary>
    /// Controller to work with files.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class FileController : ControllerBase
    {
        /// <summary>
        /// Provides information about the app's web hosting environment.
        /// </summary>
        private readonly IWebHostEnvironment _webHostEnvironment;
        /// <summary>
        /// File service.
        /// </summary>
        private readonly IFileService _fileService;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileController"/> class.
        /// </summary>
        /// <param name="webHostEnvironment">The interface of web hosting environment.</param>
        /// <param name="fileService">The interface of file service.</param>
        public FileController(IWebHostEnvironment webHostEnvironment, IFileService fileService)
        {
            _webHostEnvironment = webHostEnvironment;
            _fileService = fileService;
        }

        /// <summary>
        /// Uploads file.
        /// </summary>
        /// <param name="file">Represents a file sent with the http request.</param>
        /// <param name="userId">Id of the user who added the file.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="IActionResult"/> response object.</returns>
        [HttpPost("Upload")]
        public async Task<ResponseModel> Upload(IFormFile file, [FromQuery] int userId)
        {
            var uploads = Path.Combine(_webHostEnvironment.WebRootPath, "uploads");
            if (!Directory.Exists(uploads))
            {
                Directory.CreateDirectory(uploads);
            }
            if (file.Length > 0 && userId != 0)
            {
                foreach (string path in Directory.GetFiles(uploads))
                {
                    if (file.FileName == Path.GetFileName(path))
                    {
                        return await Task.FromResult(new ResponseModel(ResponseCode.Error, "File with such name already exists", null));
                    }
                }
                var filePath = Path.Combine(uploads, file.FileName);
                using var fileStream = new FileStream(filePath, FileMode.Create);
                await file.CopyToAsync(fileStream);
                await _fileService.AddAsync(new FileModel()
                {
                    Name = file.FileName,
                    Path = filePath,
                    UserId = userId
                });
                return await Task.FromResult(new ResponseModel(ResponseCode.OK, "", null));
            }
            return await Task.FromResult(new ResponseModel(ResponseCode.Error, "Error while uploading", null));
        }

        /// <summary>
        /// Downloads file.
        /// </summary>
        /// <param name="fileName">Represents name of the file.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="IActionResult"/> response object.</returns>
        [HttpGet("Download")]
        public async Task<IActionResult> Download([FromQuery] string fileName)
        {
            var uploads = Path.Combine(_webHostEnvironment.WebRootPath, "uploads");
            var filePath = Path.Combine(uploads, fileName);
            if (!System.IO.File.Exists(filePath))
                return NotFound();

            var memory = new MemoryStream();
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;

            return File(memory, GetContentType(filePath), fileName);
        }

        /// <summary>
        /// Gets all files.
        /// </summary>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="ResponseModel"/> response object.</returns>
        [HttpGet("Files")]
        public async Task<ResponseModel> Files()
        {
            var files = await _fileService.GetAllAsync();
            return await Task.FromResult(new ResponseModel(ResponseCode.OK, "", files));
        }

        /// <summary>
        /// Gets a specific file.
        /// </summary>
        /// <param name="fileId">Represents id.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="ResponseModel"/> response object.</returns>
        [HttpGet("File")]
        public async Task<ResponseModel> GetFile([FromQuery] int fileId)
        {
            try
            {
                var file = await _fileService.GetByIdAsync(fileId);
                return await Task.FromResult(new ResponseModel(ResponseCode.OK, "", file));
            }
            catch (FileStorageException ex)
            {
                return await Task.FromResult(new ResponseModel(ResponseCode.Error, ex.Message, null));
            }
        }

        /// <summary>
        /// Deletes file.
        /// </summary>
        /// <param name="fileId">Represents id.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="ResponseModel"/> response object.</returns>
        [HttpPost("Delete")]
        public async Task<ResponseModel> Delete([FromQuery] int fileId)
        {
            try
            {
                var file = await _fileService.GetByIdAsync(fileId);
                var uploads = Path.Combine(_webHostEnvironment.WebRootPath, "uploads");
                var filePath = Path.Combine(uploads, file.Name);

                if (!System.IO.File.Exists(filePath))
                {
                    return await Task.FromResult(new ResponseModel(ResponseCode.Error, "File not found", file));
                }
                System.IO.File.Delete(filePath);
                await _fileService.DeleteAsync(fileId);
                
                return await Task.FromResult(new ResponseModel(ResponseCode.OK, "", null));
            }
            catch(FileStorageException ex)
            {
                return await Task.FromResult(new ResponseModel(ResponseCode.Error, ex.Message, null));
            }           
        }

        /// <summary>
        /// Updates file properties.
        /// </summary>
        /// <param name="file">File data transfer object.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="ResponseModel"/> response object.</returns>
        [HttpPost("Update")]
        public async Task<ResponseModel> Update([FromBody] FileModel file)
        {
            try
            {             
                file.Path = Path.Combine(Path.Combine(_webHostEnvironment.WebRootPath, "uploads"), file.Name);
                await _fileService.UpdateAsync(file);
                return await Task.FromResult(new ResponseModel(ResponseCode.OK, "", null));
            }
            catch(FileStorageException ex)
            {
                return await Task.FromResult(new ResponseModel(ResponseCode.OK, ex.Message, null));
            }
        }

        /// <summary>
        /// Gets content type of file by given path.
        /// </summary>
        /// <param name="path">Represents full path to file.</param>
        /// <returns>File content type</returns>
        private string GetContentType(string path)
        {
            var provider = new FileExtensionContentTypeProvider();
            if (!provider.TryGetContentType(path, out string contentType))
            {
                contentType = "application/octet-stream";
            }
            return contentType;
        }
    }
}
