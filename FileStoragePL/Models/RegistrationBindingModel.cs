﻿namespace FileStoragePL.Models
{
    /// <summary>
    /// Model with properties special for registration purposes.
    /// </summary>
    public class RegistrationBindingModel
    {
        /// <summary>
        /// Stores full name.
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// Stores Email.
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Stores password.
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Stores role.
        /// </summary>
        public string Role { get; set; }
    }
}
