﻿using System.ComponentModel.DataAnnotations;

namespace FileStoragePL.Models
{
    /// <summary>
    /// Model with properties special for login purposes.
    /// </summary>
    public class LoginBindingModel
    {
        /// <summary>
        /// Stores email property.
        /// </summary>
        [Required]
        public string Email { get; set; }
        /// <summary>
        /// Stores password property.
        /// </summary>
        [Required]
        public string Password { get; set; }
        /// <summary>
        /// Stores role property.
        /// </summary>
        public string Role { get; set; }
    }
}
