﻿namespace FileStoragePL.Models
{
    /// <summary>
    /// Class that incapsulate favourable response properties in one object.
    /// </summary>
    public class ResponseModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResponseModel"/> class.
        /// </summary>
        /// <param name="responseCode">Response code to return.</param>
        /// <param name="responseMessage">Response message to pass any details.</param>
        /// <param name="dataSet">Any set of data.</param>
        public ResponseModel(ResponseCode responseCode, string responseMessage, object dataSet)
        {
            ResponseCode = responseCode;
            ResponseMessage = responseMessage;
            DataSet = dataSet;
        }
        /// <summary>
        /// Stores response code.
        /// </summary>
        public ResponseCode ResponseCode { get; set; }
        /// <summary>
        /// Stores response message.
        /// </summary>
        public string ResponseMessage { get; set; }
        /// <summary>
        /// Stores any set of data.
        /// </summary>
        public object DataSet { get; set; }
    }

    /// <summary>
    /// Stores code of response to simplify interaction.
    /// </summary>
    public enum ResponseCode
    {
        /// <summary>
        /// OK.
        /// </summary>
        OK = 1,
        /// <summary>
        /// Error.
        /// </summary>
        Error = 2
    }
}
