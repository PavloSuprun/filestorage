﻿using AutoMapper;
using FileStorageBLL.Interfaces;
using FileStorageBLL.Models;
using FileStorageBLL.Validation;
using FileStorageDAL.Entities;
using FileStorageDAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileStorageBLL.Services
{
    /// <summary>
    /// Service to work with users.
    /// </summary>
    public class AppUserService : IAppUserService
    {
        /// <summary>
        /// Unit of work.
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;
        /// <summary>
        /// AutoMapper.
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppUserService"/> class.
        /// </summary>
        /// <param name="unitOfWork">The interface of Unit Of Work.</param>
        /// <param name="mapper">The interface of AutoMapper.</param>
        public AppUserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        /// <summary>
        /// Adds <see cref="AppUserModel"/> object to Repository.
        /// </summary>
        /// <param name="model">The object of the <see cref="AppUserModel"/> data transfer object class.</param>
        /// <returns>A task that represents the asynchronous save operation.</returns>
        /// <exception cref="FileStorageException">Adding model was null.</exception>
        /// <exception cref="FileStorageException">Email has already been registered.</exception>
        public async Task AddAsync(AppUserModel model)
        {
            if (model == null)
            {
                throw new FileStorageException("Adding model was null");
            }
            if((await _unitOfWork.AppUserRepository.GetAllAsync()).FirstOrDefault(u => u.Email == model.Email) != null)
            {
                throw new FileStorageException("Email has already been registered");
            }

            await _unitOfWork.AppUserRepository.AddAsync(
                _mapper.Map<AppUser>(model));

            await _unitOfWork.SaveAsync();                     
        }

        /// <summary>
        /// Adds <see cref="AppRoleModel"/> object to Repository.
        /// </summary>
        /// <param name="appRoleModel">The object of the <see cref="AppRoleModel"/> data transfer object class.</param>
        /// <returns>A task that represents the asynchronous save operation.</returns>
        /// <exception cref="FileStorageException">Role already exists.</exception>
        public async Task AddRoleAsync(AppRoleModel appRoleModel)
        {
            if((await _unitOfWork.RoleRepository.GetAllAsync()).FirstOrDefault(r => r.RoleName.ToLower() == appRoleModel.Name.ToLower()) != null)
            {
                throw new FileStorageException($"Role {appRoleModel.Name} already exists");
            }

            await _unitOfWork.RoleRepository.AddAsync(
                _mapper.Map<AppRole>(appRoleModel)
            );

            await _unitOfWork.SaveAsync();
        }

        /// <summary>
        /// Deletes <see cref="AppUserModel"/> by its id.
        /// </summary>
        /// <param name="modelId">Represents id.</param>
        /// <returns>A task that represents the asynchronous save operation.</returns>
        /// <exception cref="FileStorageException">User with id doesn't exist.</exception>
        public async Task DeleteAsync(int modelId)
        {
            if((await _unitOfWork.AppUserRepository.GetByIdAsync(modelId)) == null)
            {
                throw new FileStorageException($"User with id {modelId} doesn't exist");
            }

            await _unitOfWork.AppUserRepository.DeleteByIdAsync(modelId);

            await _unitOfWork.SaveAsync();
        }

        /// <summary>
        /// Gets all users from Repository.
        /// </summary>
        /// <returns>
        /// A task that represents the asynchronous operation.
        /// The task result contains the <see cref="AppUserModel"/> objects gotten from the database.
        /// </returns>
        public async Task<IEnumerable<AppUserModel>> GetAllAsync()
        {
            var users = await _unitOfWork.AppUserRepository.GetAllWithDetailsAsync();

            return _mapper.Map<IEnumerable<AppUserModel>>(users);
        }

        /// <summary>
        /// Gets all roles from Repository.
        /// </summary>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="AppRoleModel.Name"/> objects gotten from the database.</returns>
        public async Task<IEnumerable<string>> GetRoles()
        {
            var roles = (await _unitOfWork.RoleRepository.GetAllAsync())
                .Select(r => r.RoleName).ToList();

            return roles;
        }

        /// <summary>
        /// Gets the id of the role by its name.
        /// </summary>
        /// <param name="roleName">Actual role name.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the Id of the role gotten from the database.</returns>
        public async Task<int> GetRoleIdByName(string roleName)
        {
            var roleId = (await _unitOfWork.RoleRepository.GetAllAsync()).FirstOrDefault(r => r.RoleName == roleName).Id; 

            return roleId;
        }

        /// <summary>
        /// Gets the role name by its id.
        /// </summary>
        /// <param name="id">Represents id.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the id of the role gotten from the database.</returns>
        public async Task<string> GetRoleNameById(int id)
        {
            var roleName = (await _unitOfWork.RoleRepository.GetAllAsync()).FirstOrDefault(r => r.Id == id).RoleName;

            return roleName;
        }

        /// <summary>
        /// Gets the <see cref="AppUserModel"/> object from Repository by id.
        /// </summary>
        /// <param name="id">Represents id.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="AppRoleModel"/> objects gotten from the database.</returns>
        /// <exception cref="FileStorageException">User with id doesn't exist.</exception>
        public async Task<AppUserModel> GetByIdAsync(int id)
        {
            var user = await _unitOfWork.AppUserRepository.GetByIdWithDetailsAsync(id);

            if(user == null)
            {
                throw new FileStorageException($"User with id {id} doesn't exist");
            }

            return _mapper.Map<AppUserModel>(user);
        }

        /// <summary>
        /// Updates user in repository.
        /// </summary>
        /// <param name="model">The update object of the <see cref="AppUserModel"/> data transfer object class.</param>
        /// <returns>A task that represents the asynchronous save operation.</returns>
        public async Task UpdateAsync(AppUserModel model)
        {
            _unitOfWork.AppUserRepository.Update(
                _mapper.Map<AppUser>(model)
            );

            await _unitOfWork.SaveAsync();
        }

        /// <summary>
        /// Performs authentication of given <see cref="AppUserModel"/> object.
        /// </summary>
        /// <param name="appUserModel">The object of the <see cref="AppUserModel"/> data transfer object class.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="AppRoleModel"/> objects gotten from the database.</returns>
        /// <exception cref="FileStorageException">Wrong credentials.</exception>
        public async Task<AppUserModel> Login(AppUserModel appUserModel)
        {
            var users = await _unitOfWork.AppUserRepository.GetAllAsync();
            var user = users.FirstOrDefault(u => u.Email == appUserModel.Email && u.Password == appUserModel.Password);

            if(user == null)
            {
                throw new FileStorageException("Wrong credentials");
            }

            return _mapper.Map<AppUserModel>(user);
        }
    }
}
