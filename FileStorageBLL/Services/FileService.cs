﻿using AutoMapper;
using FileStorageBLL.Interfaces;
using FileStorageBLL.Models;
using FileStorageBLL.Validation;
using FileStorageDAL.Data.Entities;
using FileStorageDAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileStorageBLL.Services
{
    /// <summary>
    /// Service to work with files.
    /// </summary>
    public class FileService : IFileService
    {
        /// <summary>
        /// AutoMapper.
        /// </summary>
        private readonly IMapper _mapper;
        /// <summary>
        /// Unit of work.
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileService"/> class.
        /// </summary>
        /// <param name="uow">The interface of Unit Of Work.</param>
        /// <param name="mapper">The interface of AutoMapper.</param>
        public FileService(IUnitOfWork uow, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = uow;
        }

        /// <summary>
        /// Adds <see cref="FileModel"/> object to Repository.
        /// </summary>
        /// <param name="model">The object of the <see cref="FileModel"/> data transfer object class.</param>
        /// <returns>A task that represents the asynchronous save operation.</returns>
        /// <exception cref="FileStorageException">Adding model was null</exception>
        public async Task AddAsync(FileModel model)
        {
            if(model == null)
            {
                throw new FileStorageException("Adding model was null");
            }

            await _unitOfWork.FileRepository.AddAsync(
                _mapper.Map<File>(model));

            await _unitOfWork.SaveAsync();
        }

        /// <summary>
        /// Deletes <see cref="FileModel"/> by its id.
        /// </summary>
        /// <param name="modelId">Represents id.</param>
        /// <returns>A task that represents the asynchronous save operation.</returns>
        /// <exception cref="FileStorageException">File with id doesn't exist.</exception>
        public async Task DeleteAsync(int modelId)
        {
            if ((await _unitOfWork.FileRepository.GetByIdAsync(modelId)) == null)
            {
                throw new FileStorageException($"File with id {modelId} doesn't exist");
            }

            await _unitOfWork.FileRepository.DeleteByIdAsync(modelId);

            await _unitOfWork.SaveAsync();
        }

        /// <summary>
        /// Gets all files from Repository.
        /// </summary>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="FileModel"/> objects gotten from the database.</returns>
        public async Task<IEnumerable<FileModel>> GetAllAsync()
        {
            var files = await _unitOfWork.FileRepository.GetAllWithDetailsAsync();

            return _mapper.Map<IEnumerable<FileModel>>(files);
        }

        /// <summary>
        /// Gets the <see cref="FileModel"/> object from Repository by filter.
        /// </summary>
        /// <param name="filterSearch">The object of the <see cref="FilterSearchModel"/> class.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="FileModel"/> objects gotten from the database.</returns>
        public async Task<IEnumerable<FileModel>> GetByFilterAsync(FilterSearchModel filterSearch)
        {
            var files = await GetAllAsync();

            return _mapper.Map<IEnumerable<FileModel>>(
                files.Where(f => (filterSearch.UserId == null || f.UserId == filterSearch.UserId) &&
                                 (filterSearch.Name == null || f.Name.Contains(filterSearch.Name)))
                );
        }

        /// <summary>
        /// Gets the <see cref="FileModel"/> object from Repository by id.
        /// </summary>
        /// <param name="id">Represents id.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="FileModel"/> objects gotten from the database.</returns>
        /// <exception cref="FileStorageException">User with id doesn't exist.</exception>
        public async Task<FileModel> GetByIdAsync(int id)
        {
            var file = await _unitOfWork.FileRepository.GetByIdWithDetailsAsync(id);

            if (file == null)
            {
                throw new FileStorageException($"File with id {id} doesn't exist");
            }

            return _mapper.Map<FileModel>(file);
        }

        /// <summary>
        /// Updates file in repository.
        /// </summary>
        /// <param name="model">The update object of the <see cref="FileModel"/> data transfer object class.</param>
        /// <returns>A task that represents the asynchronous save operation.</returns>
        public async Task UpdateAsync(FileModel model)
        {
            _unitOfWork.FileRepository.Update(
                _mapper.Map<File>(model)
            );

            await _unitOfWork.SaveAsync();
        }
    }
}
