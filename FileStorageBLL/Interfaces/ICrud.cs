﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileStorageBLL.Interfaces
{
    /// <summary>
    /// Generic interface of service.
    /// </summary>
    /// <typeparam name="TModel">Any class.</typeparam>
    public interface ICrud<TModel> where TModel : class
    {
        /// <summary>
        /// Gets all files.
        /// </summary>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="TModel"/> objects gotten from the database.</returns>
        public Task<IEnumerable<TModel>> GetAllAsync();

        /// <summary>
        /// Gets the <see cref="TModel"/> object by id.
        /// </summary>
        /// <param name="id">Represents id.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="TModel"/> objects gotten from the database.</returns>
        public Task<TModel> GetByIdAsync(int id);

        /// <summary>
        /// Adds <see cref="TModel"/> object.
        /// </summary>
        /// <param name="model">The object of the <see cref="TModel"/> data transfer object class.</param>
        /// <returns>A task that represents the asynchronous save operation.</returns>
        public Task AddAsync(TModel model);

        /// <summary>
        /// Updates <see cref="TModel"/> object.
        /// </summary>
        /// <param name="model">The update object of the <see cref="TModel"/>.</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        public Task UpdateAsync(TModel model);

        /// <summary>
        /// Deletes <see cref="TModel"/> by its id.
        /// </summary>
        /// <param name="modelId">Represents id.</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        public Task DeleteAsync(int modelId);
    }
}
