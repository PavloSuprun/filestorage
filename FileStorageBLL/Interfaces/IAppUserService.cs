﻿using FileStorageBLL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileStorageBLL.Interfaces
{
    /// <summary>
    /// Interface of service to work with users.
    /// </summary>
    public interface IAppUserService : ICrud<AppUserModel>
    {
        /// <summary>
        /// Adds <see cref="AppRoleModel"/> object.
        /// </summary>
        /// <param name="appRoleModel">The object of the <see cref="AppRoleModel"/> data transfer object class.</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        public Task AddRoleAsync(AppRoleModel appRoleModel);

        /// <summary>
        /// Gets all roles.
        /// </summary>
        /// <returns>A task that represents the asynchronous operation.</returns>
        public Task<IEnumerable<string>> GetRoles();

        /// <summary>
        /// Performs authentication of given <see cref="AppUserModel"/> object.
        /// </summary>
        /// <param name="appUserModel">The object of the <see cref="AppUserModel"/> data transfer object class.</param>
        /// <returns>The task result contains the <see cref="AppRoleModel"/> objects gotten from the database.</returns>
        public Task<AppUserModel> Login(AppUserModel appUserModel);

        /// <summary>
        /// Gets the id of the role by its name.
        /// </summary>
        /// <param name="roleName">Actual role name.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the id of the role gotten from the database.</returns>
        public Task<int> GetRoleIdByName(string roleName);

        /// <summary>
        /// Gets the role name by its id.
        /// </summary>
        /// <param name="id">Represents id.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the id of the role gotten from the database.</returns>
        public Task<string> GetRoleNameById(int id);
    }
}
