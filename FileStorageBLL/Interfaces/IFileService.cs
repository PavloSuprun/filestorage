﻿using FileStorageBLL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileStorageBLL.Interfaces
{
    /// <summary>
    /// Interface of service to work with file.
    /// </summary>
    public interface IFileService : ICrud<FileModel>
    {
        /// <summary>
        /// Gets the <see cref="FileModel"/> object by filter.
        /// </summary>
        /// <param name="filterSearch">The object of the <see cref="FilterSearchModel"/> class.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="FileModel"/> objects gotten from the database.</returns>
        Task<IEnumerable<FileModel>> GetByFilterAsync(FilterSearchModel filterSearch);
    }
}
