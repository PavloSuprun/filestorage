﻿using AutoMapper;
using FileStorageBLL.Models;
using FileStorageDAL.Data.Entities;
using FileStorageDAL.Entities;
using System.Linq;

namespace FileStorageBLL.Mapper
{
    /// <summary>
    /// Provides custom automapper profiles.
    /// </summary>
    public class AutomapperProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AutomapperProfile"/> class.
        /// </summary>
        public AutomapperProfile()
        {
            CreateMap<AppUser, AppUserModel>()
                .ForMember(um => um.FilesIds, x => x.MapFrom(u => u.Files.Select(f => f.Id)))
                .ForMember(um => um.RoleId, x => x.MapFrom(u => u.RoleId));

            CreateMap<AppUserModel, AppUser>()
                .ForMember(u => u.RoleId, x => x.MapFrom(um => um.RoleId));

            CreateMap<File, FileModel>()
                .ForMember(fm => fm.UserId, x => x.MapFrom(f => f.AppUserId));

            CreateMap<FileModel, File>()
                .ForMember(f => f.AppUserId, x => x.MapFrom(fm => fm.UserId));

            CreateMap<AppRole, AppRoleModel>()
                .ForMember(arm => arm.AppUserIds, x => x.MapFrom(ar => ar.Users.Select(u => u.Id)));

            CreateMap<AppRoleModel, AppRole>()
                .ForMember(ar => ar.RoleName, x => x.MapFrom(arm => arm.Name));
                
        }
    }
}
