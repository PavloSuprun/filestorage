﻿using System;
using System.Runtime.Serialization;

namespace FileStorageBLL.Validation
{
    /// <summary>
    /// File storage custom exception class.
    /// </summary>
    [Serializable()]
    public class FileStorageException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileStorageException"/> class.
        /// </summary>
        public FileStorageException() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileStorageException"/> class.
        /// </summary>
        /// <param name="message">Exception details message</param>
        public FileStorageException(string message) : base(message) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileStorageException"/> class.
        /// </summary>
        /// <param name="message">Exception details message</param>
        /// <param name="inner">Inner exception</param>
        public FileStorageException(string message, Exception inner) : base(message, inner) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileStorageException"/> class.
        /// </summary>
        /// <param name="info">Serialization information</param>
        /// <param name="context">Streaming context</param>
        protected FileStorageException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
