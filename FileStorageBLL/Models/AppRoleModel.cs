﻿using System.Collections.Generic;

namespace FileStorageBLL.Models
{
    /// <summary>
    /// User role data transfer object class.
    /// </summary>
    public class AppRoleModel
    {
        /// <summary>
        /// Stores role id.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Stores role name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Stores collection of users of current id.
        /// </summary>
        public ICollection<int> AppUserIds { get; set; }
    }
}
