﻿namespace FileStorageBLL.Models
{
    /// <summary>
    /// Filtering class.
    /// </summary>
    public class FilterSearchModel
    {
    #nullable enable
        /// <summary>
        /// Stores int id.
        /// </summary>
        public int? UserId { get; set; }
        /// <summary>
        /// Stores string name.
        /// </summary>
        public string? Name { get; set; }
    #nullable disable
    }
}
