﻿using System.Collections.Generic;

namespace FileStorageBLL.Models
{
    /// <summary>
    /// User data transfer object class.
    /// </summary>
    public class AppUserModel
    {
        /// <summary>
        /// Stores user id.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Stores user full name.
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// Stores user email.
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Stores user password.
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Stores user role.
        /// </summary>
        public int RoleId { get; set; }
        /// <summary>
        /// Stores authorization token.
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// Stores files ids of current user.
        /// </summary>
        public ICollection<int> FilesIds { get; set; }
    }
}
