﻿namespace FileStorageBLL.Models
{
    /// <summary>
    /// File data transfer object class.
    /// </summary>
    public class FileModel
    {
        /// <summary>
        /// Stores file id.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Stores file name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Stores file path.
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Stores id of user that added current file.
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Stores true if file is public, false - private.
        /// </summary>
        public bool IsPublic { get; set; }

    }
}
