﻿using FileStorageDAL.Interfaces;
using FileStorageDAL.Repositories;
using System.Threading.Tasks;

namespace FileStorageDAL.Data
{
    /// <summary>
    /// Coordinates the work of multiple repositories.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// DbContext.
        /// </summary>
        private readonly FileStorageDbContext _context;
        /// <summary>
        /// AppUsers repository.
        /// </summary>
        private IAppUserRepository _appUserRepository;
        /// <summary>
        /// Files repository.
        /// </summary>
        private IFileRepository _fileRepository;
        /// <summary>
        /// Roles repository.
        /// </summary>
        public IRoleRepository _roleRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork"/> class.
        /// </summary>
        /// <param name="db">DbContext.</param>
        public UnitOfWork(FileStorageDbContext db)
        {
            _context = db;
        }

        /// <summary>
        /// Gets the instance of the <see cref="IAppUserRepository"/>.
        /// </summary>
        public IAppUserRepository AppUserRepository
        {
            get
            {
                if(_appUserRepository == null)
                {
                    _appUserRepository = new AppUserRepository(_context);
                }
                return _appUserRepository;
            }
        }

        /// <summary>
        /// Gets the instance of the <see cref="IFileRepository"/>.
        /// </summary>
        public IFileRepository FileRepository
        {
            get
            {
                if (_fileRepository == null)
                {
                    _fileRepository = new FileRepository(_context);
                }
                return _fileRepository;
            }
        }

        /// <summary>
        /// Gets the instance of the <see cref="IRoleRepository"/>.
        /// </summary>
        public IRoleRepository RoleRepository
        {
            get
            {
                if (_roleRepository == null)
                {
                    _roleRepository = new AppRoleRepository(_context);
                }
                return _roleRepository;
            }
        }

        /// <summary>
        /// Saves all changes to database.
        /// </summary>
        /// <returns>A task that represents the asynchronous save operation.</returns>
        async Task IUnitOfWork.SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
