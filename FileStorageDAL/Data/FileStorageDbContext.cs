﻿using FileStorageDAL.Data.Entities;
using FileStorageDAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace FileStorageDAL.Data
{
    /// <summary>
    /// File storage application DbContext class.
    /// </summary>
    public class FileStorageDbContext : DbContext
    {
        /// <summary>
        /// DbSet collection of <see cref="File"/>s.
        /// </summary>
        public DbSet<File> Files { get; set; }
        /// <summary>
        /// DbSet collection of <see cref="AppUser"/>s.
        /// </summary>
        public DbSet<AppUser> AppUsers { get; set; }
        /// <summary>
        /// DbSet collection of <see cref="AppRole"/>s.
        /// </summary>
        public DbSet<AppRole> AppRoles { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileStorageDbContext"/> class.
        /// </summary>
        /// <param name="options">The options to be used by a DbContext.</param>
        public FileStorageDbContext(DbContextOptions options) : base(options) { }

        /// <summary>
        /// Configures the models.
        /// </summary>
        /// <param name="modelBuilder">Provides a simple api for configuring a model relationships.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppUser>()
                .HasMany(u => u.Files)
                .WithOne(f => f.AppUser)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<AppUser>()
                .HasOne(u => u.Role)
                .WithMany(r => r.Users)
                .HasForeignKey(u => u.RoleId);

            base.OnModelCreating(modelBuilder);
        }
    }
}
