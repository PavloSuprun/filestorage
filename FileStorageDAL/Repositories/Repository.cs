﻿using FileStorageDAL.Data;
using FileStorageDAL.Entities;
using FileStorageDAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileStorageDAL.Repositories
{
    /// <summary>
    /// Generic abstract repository class.
    /// </summary>
    /// <typeparam name="T">Any entity which inherits from BaseEntity.</typeparam>
    public abstract class Repository<T> : IRepository<T> where T : BaseEntity
    {
        /// <summary>
        /// DbContext.
        /// </summary>
        protected FileStorageDbContext Context { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// </summary>
        /// <param name="context">DbContext.</param>
        protected Repository(FileStorageDbContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Adds <see cref="T"/> object to database.
        /// </summary>
        /// <param name="entity">The object of <see cref="T"/>.</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        public async Task AddAsync(T entity)
        {
            await Context.Set<T>().AddAsync(entity);
        }

        /// <summary>
        /// Deletes <see cref="T"/> object from database.
        /// </summary>
        /// <param name="entity">The object of <see cref="T"/>.</param>
        public void Delete(T entity)
        {
            Context.Set<T>().Remove(entity);
        }

        /// <summary>
        /// Deletes <see cref="T"/> object from database by its id.
        /// </summary>
        /// <param name="id">The object of <see cref="T"/>.</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        public async Task DeleteByIdAsync(int id)
        {
            Context.Set<T>().Remove(Context.Set<T>().FirstOrDefault(x => x.Id == id));
            await Context.SaveChangesAsync();
        }

        /// <summary>
        /// Returns all items of one type from database.
        /// </summary>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="T"/> objects gotten from the database.</returns>
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Context.Set<T>().ToListAsync();
        }

        /// <summary>
        /// Returns the <see cref="T"/> object from database by the id.
        /// </summary>
        /// <param name="id">Represents entity's id.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="T"/> object gotten from the database.</returns>
        public async Task<T> GetByIdAsync(int id)
        {
            return await Context.Set<T>().FindAsync(id);
        }

        /// <summary>
        /// Updates <see cref="T"/> object in database.
        /// </summary>
        /// <param name="entity">The object of <see cref="T"/>.</param>
        public void Update(T entity)
        {
            Context.Set<T>().Update(entity);
        }
    }
}
