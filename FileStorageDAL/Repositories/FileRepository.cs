﻿using FileStorageDAL.Data;
using FileStorageDAL.Data.Entities;
using FileStorageDAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileStorageDAL.Repositories
{
    /// <summary>
    /// File repository class.
    /// </summary>
    public class FileRepository : Repository<File>, IFileRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileRepository"/> class.
        /// </summary>
        /// <param name="context">DbContext.</param>
        public FileRepository(FileStorageDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Gets all files with user added.
        /// </summary>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="File"/> objects gotten from the database.</returns>
        public async Task<IEnumerable<File>> GetAllWithDetailsAsync()
        {
            return await Context.Files
                .Include(f => f.AppUser)
                .ToListAsync();
        }

        /// <summary>
        /// Gets file with user added by given id.
        /// </summary>
        /// <param name="id">Represents id.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="File"/> object gotten from the database.</returns>
        public async Task<File> GetByIdWithDetailsAsync(int id)
        {
            return await Context.Files
                .Include(f => f.AppUser)
                .FirstOrDefaultAsync(f => f.Id == id);
        }
    }
}
