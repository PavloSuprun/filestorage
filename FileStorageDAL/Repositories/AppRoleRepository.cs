﻿using FileStorageDAL.Data;
using FileStorageDAL.Entities;
using FileStorageDAL.Interfaces;

namespace FileStorageDAL.Repositories
{
    /// <summary>
    /// Role repository class.
    /// </summary>
    public class AppRoleRepository : Repository<AppRole>, IRoleRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppRoleRepository"/> class.
        /// </summary>
        /// <param name="context">DbContext.</param>
        public AppRoleRepository(FileStorageDbContext context) : base(context)
        {
        }
    }
}
