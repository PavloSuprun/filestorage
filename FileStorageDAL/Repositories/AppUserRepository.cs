﻿using FileStorageDAL.Data;
using FileStorageDAL.Entities;
using FileStorageDAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileStorageDAL.Repositories
{
    /// <summary>
    /// User repository class.
    /// </summary>
    public class AppUserRepository : Repository<AppUser>, IAppUserRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppUserRepository"/> class.
        /// </summary>
        /// <param name="context">DbContext.</param>
        public AppUserRepository(FileStorageDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Gets all users with files.
        /// </summary>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="AppUser"/> objects gotten from the database.</returns>
        public async Task<IEnumerable<AppUser>> GetAllWithDetailsAsync()
        {
            return await Context.AppUsers
                .Include(u => u.Files)
                .ToListAsync();
        }

        /// <summary>
        /// Gets user with files by given id.
        /// </summary>
        /// <param name="id">Represents id.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="AppUser"/> object gotten from the database.</returns>
        public async Task<AppUser> GetByIdWithDetailsAsync(int id)
        {
            return await Context.AppUsers
                .Include(u => u.Files)
                .FirstOrDefaultAsync();
        }
    }
}
