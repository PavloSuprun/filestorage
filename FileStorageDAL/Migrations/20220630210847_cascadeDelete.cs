﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FileStorageDAL.Migrations
{
    public partial class cascadeDelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Files_AppUsers_AppUserId",
                table: "Files");

            migrationBuilder.AddForeignKey(
                name: "FK_Files_AppUsers_AppUserId",
                table: "Files",
                column: "AppUserId",
                principalTable: "AppUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Files_AppUsers_AppUserId",
                table: "Files");

            migrationBuilder.AddForeignKey(
                name: "FK_Files_AppUsers_AppUserId",
                table: "Files",
                column: "AppUserId",
                principalTable: "AppUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
