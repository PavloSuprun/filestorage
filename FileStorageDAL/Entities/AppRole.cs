﻿using System.Collections.Generic;

namespace FileStorageDAL.Entities
{
    /// <summary>
    /// Represents user role.
    /// </summary>
    public class AppRole : BaseEntity
    {
        /// <summary>
        /// Stores name of the role.
        /// </summary>
        public string RoleName { get; set; }
        /// <summary>
        /// Stores collection of users of that role.
        /// </summary>
        public ICollection<AppUser> Users { get; set; }
    }
}
