﻿using FileStorageDAL.Entities;

namespace FileStorageDAL.Data.Entities
{
    /// <summary>
    /// Represents file.
    /// </summary>
    public class File : BaseEntity
    {
        /// <summary>
        /// Stores name of the file.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Stores path to file.
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Stores user id who added this file.
        /// </summary>
        public int? AppUserId { get; set; }
        /// <summary>
        /// Navigation property. File HAS-a user.
        /// </summary>
        public AppUser AppUser { get; set; }
        /// <summary>
        /// Stores true if file is public, false - private.
        /// </summary>
        public bool IsPublic { get; set; }
    }
}
