﻿namespace FileStorageDAL.Entities
{
    /// <summary>
    /// Base class of entity with common parameters.
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// Unique ientifier.
        /// </summary>
        public int Id { get; set; }
    }
}
