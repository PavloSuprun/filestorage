﻿using FileStorageDAL.Data.Entities;
using System.Collections.Generic;

namespace FileStorageDAL.Entities
{
    /// <summary>
    /// Represents user.
    /// </summary>
    public class AppUser : BaseEntity
    {
        /// <summary>
        /// Stores full name.
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// Stores email.
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Stores password.
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Stores role id.
        /// </summary>
        public int RoleId { get; set; }
        /// <summary>
        /// Navigation property. User HAS-a role.
        /// </summary>
        public AppRole Role { get; set; }
        /// <summary>
        /// Stores collection of files of the user.
        /// </summary>
        public ICollection<File> Files { get; set; }
    }
}
