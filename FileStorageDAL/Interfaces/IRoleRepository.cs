﻿using FileStorageDAL.Entities;

namespace FileStorageDAL.Interfaces
{
    /// <summary>
    /// Interface of role repository.
    /// </summary>
    public interface IRoleRepository : IRepository<AppRole>
    {
    }
}
