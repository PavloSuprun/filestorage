﻿using FileStorageDAL.Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileStorageDAL.Interfaces
{
    /// <summary>
    /// Interface of file repository.
    /// </summary>
    public interface IFileRepository : IRepository<File>
    {
        /// <summary>
        /// Gets all files with user added.
        /// </summary>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="File"/> objects gotten from the database.</returns>
        Task<IEnumerable<File>> GetAllWithDetailsAsync();

        /// <summary>
        /// Gets file with user added by given id.
        /// </summary>
        /// <param name="id">Represents id.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="File"/> object gotten from the database.</returns>
        Task<File> GetByIdWithDetailsAsync(int id);
    }
}
