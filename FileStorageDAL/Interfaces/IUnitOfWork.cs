﻿using System.Threading.Tasks;

namespace FileStorageDAL.Interfaces
{
    /// <summary>
    /// Interface of unit of work.
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// User repository interface.
        /// </summary>
        IAppUserRepository AppUserRepository { get; }
        /// <summary>
        /// File repository interface.
        /// </summary>
        IFileRepository FileRepository { get; }
        /// <summary>
        /// Role repository interface.
        /// </summary>
        IRoleRepository RoleRepository { get; }
        /// <summary>
        /// Saves all changes to database.
        /// </summary>
        /// <returns>A task that represents the asynchronous operation.</returns>
        Task SaveAsync();
    }
}
