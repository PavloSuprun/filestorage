﻿using FileStorageDAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileStorageDAL.Interfaces
{
    /// <summary>
    /// Interface of user repository
    /// </summary>
    public interface IAppUserRepository : IRepository<AppUser>
    {
        /// <summary>
        /// Gets all users with files.
        /// </summary>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="AppUser"/> objects gotten from the database.</returns>
        Task<IEnumerable<AppUser>> GetAllWithDetailsAsync();

        /// <summary>
        /// Gets user with files by given id.
        /// </summary>
        /// <param name="id">Represents id.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="AppUser"/> object gotten from the database.</returns>
        Task<AppUser> GetByIdWithDetailsAsync(int id);
    }
}
