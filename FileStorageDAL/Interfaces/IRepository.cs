﻿using FileStorageDAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileStorageDAL.Interfaces
{
    /// <summary>
    /// Generic interface of repository.
    /// </summary>
    /// <typeparam name="TEntity">Any entity which inherits from BaseEntity.</typeparam>
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        /// <summary>
        /// Returns all items of one type from database.
        /// </summary>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="TEntity"/> objects gotten from the database.</returns>
        Task<IEnumerable<TEntity>> GetAllAsync();

        /// <summary>
        /// Returns the <see cref="TEntity"/> object from database by the id.
        /// </summary>
        /// <param name="id">Represents entity's id.</param>
        /// <returns>A task that represents the asynchronous operation.
        /// The task result contains the <see cref="TEntity"/> object gotten from the database.</returns>
        Task<TEntity> GetByIdAsync(int id);

        /// <summary>
        /// Adds <see cref="TEntity"/> object to database.
        /// </summary>
        /// <param name="entity">The object of <see cref="TEntity"/>.</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        Task AddAsync(TEntity entity);

        /// <summary>
        /// Deletes <see cref="TEntity"/> object from database.
        /// </summary>
        /// <param name="entity">The object of <see cref="TEntity"/>.</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Deletes <see cref="TEntity"/> object from database by its id.
        /// </summary>
        /// <param name="id">The object of <see cref="TEntity"/>.</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        Task DeleteByIdAsync(int id);

        /// <summary>
        /// Updates <see cref="TEntity"/> object in database.
        /// </summary>
        /// <param name="entity">The object of <see cref="TEntity"/>.</param>
        void Update(TEntity entity);
    }
}
